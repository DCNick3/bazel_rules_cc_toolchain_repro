FROM archlinux

RUN true \
    && pacman -Sy --noconfirm gcc git binutils make ncurses fakeroot sudo \
    && useradd -m user \
    && cd /home/user \
    && sudo -u user git clone https://aur.archlinux.org/bazelisk-bin.git \
    && cd bazelisk-bin \
    && sudo -u user makepkg \
    && pacman -U --noconfirm bazelisk-bin-*.pkg.* \
    && cd .. \
    && sudo -u user git clone https://aur.archlinux.org/ncurses5-compat-libs.git \
    && cd ncurses5-compat-libs \
    && sudo -u user makepkg --skippgpcheck \
    && pacman -U --noconfirm ncurses5-compat-libs-*.pkg.*

WORKDIR /root
ENTRYPOINT ["bash"]
